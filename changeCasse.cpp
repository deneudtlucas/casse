// changeCasse.cpp
// g++ -std=c++11 -Wall -Wextra -o changeCasse.out changeCasse.cpp
#include <cctype>
#include <iostream>
using namespace std;

int main()
{
    string text;
    getline(cin, text);
    // change les minuscules par des majuscules et réciproquement
    for (char & c : text)
    {
        if (islower(c))
            cout << char(toupper(c));
        else if (isupper(c))
            cout << char(tolower(c));
        else 
            cout << c;
    }
    cout << "\n";
    return 0;
}
